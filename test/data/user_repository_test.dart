import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';
import 'package:hello_friends/data/repository/user_repository.dart';
import 'package:hello_friends/domain/entity/user_entity_login.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesMock extends Mock implements SharedPreferences{
}

void main(){
  group("Test repository ", () {

    const user =UserEntityLogin(nombre: "nombre", correo: "correo", phone: "phone", password: "password");
    final share=SharedPreferencesMock();
    test("check save User when is a Rigth an save the user funcion", ()  async {
      final repository= UserRepositoryImpl(preferences: share);

      when(() => share.getString(any()) ).thenReturn(jsonEncode(user.toMap()));
      when(() => share.setString(any(), any()))
          .thenAnswer((_) async => true);
      final result= await  repository.saveUser(user);
      final valied= result.isLeft;
      expect(valied,false);
    });

    test("check save User when is a Left funcion second", ()  async {
      final repository= UserRepositoryImpl(preferences: share);

      when(() => share.getString(any()) ).thenReturn(null);
      when(() => share.setString(any(), any()))
          .thenAnswer((_) async => true);
      final result= await  repository.saveUser(user);
      final valied= result.isLeft;
      expect(valied,true);
    });
  });


}



