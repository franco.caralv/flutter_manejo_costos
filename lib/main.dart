import 'package:flutter/material.dart';
import 'package:hello_friends/container.dart';
import 'package:hello_friends/controller/principle_view_model.dart';
import 'package:hello_friends/view/login_screen.dart';
import 'package:hello_friends/view/principle_screen.dart';
import 'package:hello_friends/view/counter.dart';
import 'package:injector/injector.dart';
import 'package:provider/provider.dart';
main()  {
  runApp(FutureBuilder(future:  container(), builder:(BuildContext context, AsyncSnapshot snapshot) {
     if (snapshot.hasData) {
       return  MultiProvider(
         providers: [
           ChangeNotifierProvider(create: (_) => Injector.appInstance.get<PrincipleViewModel>(),lazy: true,),
         ],
         child: const MyApp(),
       );
     } else{
       return const CircularProgressIndicator();
     }

  }));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: const  ColorScheme.dark(primary: Color.fromRGBO(92,222,143,1),
          background:  Color.fromRGBO(18, 23, 20, 1),
          secondary: Color.fromRGBO(41,56,46,1 ),
          outline: Color.fromRGBO(41,56,46,1 ),
        ),
        textTheme: Typography.whiteCupertino,
        primaryTextTheme: Typography.whiteCupertino,
        useMaterial3: true,
        appBarTheme: const AppBarTheme(backgroundColor: Color.fromRGBO(18, 23, 20, 1))
      ),
      initialRoute: "/detail",
      routes:  {
        '/home': (context) => const CounterScreen(),
        '/detail': (context) => const CounterScreen(),
        "/login": (context) => const LoginScreen()
      },
    );

  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        backgroundColor: Theme.of(context).colorScheme.inversePrimary,

        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed tdhe abuttond this many tiCSXmees:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),

      ),
    );
  }
}
