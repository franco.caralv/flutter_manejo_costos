import 'package:either_dart/either.dart';
import 'package:flutter/material.dart';
import 'package:hello_friends/controller/data/password_field.dart';
import 'package:hello_friends/domain/entity/user_entity_login.dart';
import 'package:hello_friends/domain/excepcion/use_case_error_save.dart';
import 'package:hello_friends/domain/usecases/use_case_user_save.dart';

class PrincipleViewModel extends ChangeNotifier {
  final TextEditingController name = TextEditingController();
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();
  var repeatPassword =
      PassWordField(text: TextEditingController(), isSeePass: true);
  var password = PassWordField(text: TextEditingController(), isSeePass: true);

  final UseCaseUserSave useCaseUserSave;

  PrincipleViewModel({required this.useCaseUserSave});

  changeSeePassword() {
    password = password.copyWith(isSeePass: !password.isSeePass);
    notifyListeners();
  }

  changeSeeRepeatPassword() {
    repeatPassword =
        repeatPassword.copyWith(isSeePass: !repeatPassword.isSeePass);
    notifyListeners();
  }

  Future<Either<String, void>> saveUser() async {

    if (repeatPassword.text.text !=
        password.text.text) {
      return const Left("Las Contraseñas no coinciden");
    }

    final saveUser = await useCaseUserSave.saveUser(
        user: UserEntityLogin(
            correo: email.text,
            nombre: name.text,
            password: password.text.text,
            phone: phone.text));
    switch (saveUser) {

      case Left():
        notifyListeners();
        return const Left("Las Contraseñas no coinciden");
      case Right():
        notifyListeners();
        return const Right(null);
    }

  }
}
