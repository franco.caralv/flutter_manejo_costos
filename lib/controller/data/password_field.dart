import 'package:flutter/widgets.dart';

class PassWordField {
  TextEditingController text;
  bool isSeePass;

//<editor-fold desc="Data Methods">
  PassWordField({
    required this.text,
    required this.isSeePass,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PassWordField &&
          runtimeType == other.runtimeType &&
          text == other.text &&
          isSeePass == other.isSeePass);

  @override
  int get hashCode => text.hashCode ^ isSeePass.hashCode;

  @override
  String toString() {
    return 'PassWordField{' + ' text: $text,' + ' isSeePass: $isSeePass,' + '}';
  }

  PassWordField copyWith({
    TextEditingController? text,
    bool? isSeePass,
  }) {
    return PassWordField(
      text: text ?? this.text,
      isSeePass: isSeePass ?? this.isSeePass,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'text': text,
      'isSeePass': isSeePass,
    };
  }

  factory PassWordField.fromMap(Map<String, dynamic> map) {
    return PassWordField(
      text: map['text'] as TextEditingController,
      isSeePass: map['isSeePass'] as bool,
    );
  }

//</editor-fold>
}