
import 'package:either_dart/either.dart';
import 'package:hello_friends/domain/entity/user_entity_login.dart';
import 'package:hello_friends/domain/excepcion/server_excepcion.dart';

abstract class UserRepository{


  Future<Either<ServerExcepcion,UserEntityLogin>> saveUser(UserEntityLogin user);


}