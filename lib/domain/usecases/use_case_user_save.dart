import 'package:either_dart/either.dart';
import 'package:hello_friends/domain/entity/user_entity_login.dart';
import 'package:hello_friends/domain/excepcion/use_case_error_save.dart';
import 'package:hello_friends/domain/repository/user_repository.dart';

class UseCaseUserSave{
  final UserRepository _userRepository;

  UseCaseUserSave({
      required UserRepository userRepository
  }): _userRepository=userRepository;

  Future<Either<Left<UseCaseErrorSave, dynamic>, Right<dynamic, Null>>>  saveUser({
    required UserEntityLogin user
    }) async {
    final result = await _userRepository.saveUser(user);

    return result.mapLeft((left) => Left(UseCaseErrorSave())).map((right) =>const Right(null) );

  }





}


