

class UserEntityLogin{
  final String  nombre;
  final String  correo;
  final String  phone;
  final String  password;

//<editor-fold desc="Data Methods">
  const UserEntityLogin({
    required this.nombre,
    required this.correo,
    required this.phone,
    required this.password,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is UserEntityLogin &&
          runtimeType == other.runtimeType &&
          nombre == other.nombre &&
          correo == other.correo &&
          phone == other.phone &&
          password == other.password);

  @override
  int get hashCode =>
      nombre.hashCode ^ correo.hashCode ^ phone.hashCode ^ password.hashCode;

  @override
  String toString() {
    return 'UserEntityLogin{' +
        ' nombre: $nombre,' +
        ' correo: $correo,' +
        ' phone: $phone,' +
        ' password: $password,' +
        '}';
  }

  UserEntityLogin copyWith({
    String? nombre,
    String? correo,
    String? phone,
    String? password,
  }) {
    return UserEntityLogin(
      nombre: nombre ?? this.nombre,
      correo: correo ?? this.correo,
      phone: phone ?? this.phone,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nombre': this.nombre,
      'correo': this.correo,
      'phone': this.phone,
      'password': this.password,
    };
  }

  factory UserEntityLogin.fromMap(Map<String, dynamic> map) {
    return UserEntityLogin(
      nombre: map['nombre'] as String,
      correo: map['correo'] as String,
      phone: map['phone'] as String,
      password: map['password'] as String,
    );
  }

//</editor-fold>
}