

class UserDataLogin{

   final String  nombre;
   final String  correo;
   final String  phone;
   final String  password;

//<editor-fold desc="Data Methods">
  const UserDataLogin({
    required this.nombre,
    required this.correo,
    required this.phone,
    required this.password,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is UserDataLogin &&
          runtimeType == other.runtimeType &&
          nombre == other.nombre &&
          correo == other.correo &&
          phone == other.phone &&
          password == other.password);

  @override
  int get hashCode =>
      nombre.hashCode ^ correo.hashCode ^ phone.hashCode ^ password.hashCode;

  @override
  String toString() {
    return 'UserDataLogin{' +
        ' nombre: $nombre,' +
        ' correo: $correo,' +
        ' phone: $phone,' +
        ' password: $password,' +
        '}';
  }

  UserDataLogin copyWith({
    String? nombre,
    String? correo,
    String? phone,
    String? password,
  }) {
    return UserDataLogin(
      nombre: nombre ?? this.nombre,
      correo: correo ?? this.correo,
      phone: phone ?? this.phone,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nombre': this.nombre,
      'correo': this.correo,
      'phone': this.phone,
      'password': this.password,
    };
  }

  factory UserDataLogin.fromMap(Map<String, dynamic> map) {
    return UserDataLogin(
      nombre: map['nombre'] as String,
      correo: map['correo'] as String,
      phone: map['phone'] as String,
      password: map['password'] as String,
    );
  }

//</editor-fold>
}