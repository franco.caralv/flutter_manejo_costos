

import 'dart:convert';

import 'package:either_dart/src/either.dart' show Either, Left, Right;
import 'package:hello_friends/domain/entity/user_entity_login.dart';
import 'package:hello_friends/domain/excepcion/server_excepcion.dart';
import 'package:hello_friends/domain/repository/user_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserRepositoryImpl implements  UserRepository{

  static const _keyuser="user";

  SharedPreferences _preferences;

  UserRepositoryImpl( {
    required SharedPreferences  preferences
}): _preferences= preferences;

  @override
  Future<Either<ServerExcepcion, UserEntityLogin>> saveUser(UserEntityLogin user) async {
    final SharedPreferences prefs = _preferences;
    try {
      await prefs.setString(user.nombre+user.password,jsonEncode( user.toMap()));
    } catch (e) {
      return Left(ServerExcepcion());
    }
    final userinit=  prefs.getString(user.nombre+user.password);
    if (userinit == null){
      return Left(ServerExcepcion());
    }
    final userMap = jsonDecode(userinit) as Map<String, dynamic>;
    final newUser  = UserEntityLogin.fromMap(userMap);
    return Right(newUser);
  }


  Future<Either<ServerExcepcion, UserEntityLogin>> getUser(UserEntityLogin user) async {
    final SharedPreferences prefs = _preferences;
    try {
      await prefs.setString(user.nombre+user.password,jsonEncode( user.toMap()));
    } catch (e) {
      return Left(ServerExcepcion());
    }
    final userinit=  prefs.getString(user.nombre+user.password);
    if (userinit == null){
      return Left(ServerExcepcion());
    }
    final userMap = jsonDecode(userinit) as Map<String, dynamic>;
    final newUser  = UserEntityLogin.fromMap(userMap);
    return Right(newUser);
  }

}