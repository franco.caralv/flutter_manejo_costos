import 'package:flutter/widgets.dart';
import 'package:hello_friends/controller/principle_view_model.dart';
import 'package:hello_friends/data/repository/user_repository.dart';
import 'package:hello_friends/domain/usecases/use_case_user_save.dart';
import 'package:injector/injector.dart';
import 'package:shared_preferences/shared_preferences.dart';



Future<bool> container() async {
  WidgetsFlutterBinding.ensureInitialized();
  final injector = Injector.appInstance;
  final preferences= await SharedPreferences.getInstance();
  injector.registerSingleton(() => preferences);
  injector.registerSingleton(
      () => UserRepositoryImpl(preferences: preferences));
  injector.registerSingleton(() =>
      UseCaseUserSave(userRepository: injector.get<UserRepositoryImpl>()));
  injector.registerSingleton(() =>
      PrincipleViewModel(useCaseUserSave: injector.get<UseCaseUserSave>()));


  return true;
}
