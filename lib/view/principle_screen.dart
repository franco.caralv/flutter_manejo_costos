import 'package:either_dart/either.dart';
import 'package:flutter/material.dart';
import 'package:hello_friends/controller/principle_view_model.dart';
import 'package:hello_friends/view/counter.dart';
import 'package:hello_friends/view/login_screen.dart';
import 'package:provider/provider.dart';

final class PrincipleScreen extends StatelessWidget {
  const PrincipleScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final formkey = context.select((PrincipleViewModel value) => value.formkey);
    final email = context.select((PrincipleViewModel value) => value.email);
    final password =
        context.select((PrincipleViewModel value) => value.password);
    final repeatPassword =
        context.select((PrincipleViewModel value) => value.repeatPassword);
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Registro",
            textAlign: TextAlign.center,
            selectionColor: Colors.white,
          ),
          centerTitle: true,
          elevation: 4.0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            tooltip: 'Go back',
            onPressed: () {
              if (Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              } else {
                Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => const CounterScreen()),
                  (Route<dynamic> route) => false,
                );
              }
            },
          ),
        ),
        body: ListView(
          children: [
            Container(
              height: 200,
              child: Center(
                child: Image.asset('images/presente.png'),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Form(
                  key: formkey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const FieldName(),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).size.height.round() *
                                0.03),
                        child: TextFormField(
                            decoration: const InputDecoration(
                              labelText: "Correo",
                              border: OutlineInputBorder(),
                              suffixIcon: Icon(Icons.email),
                            ),
                            obscureText: false,
                            controller: email,
                            keyboardType: TextInputType.emailAddress,
                            onChanged: (va) {
                              formkey.currentState!.validate();
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Por favor ingrese su correo electrónico';
                              } else if (!value.contains('@')) {
                                return 'Por favor ingrese un correo electrónico válido';
                              }
                              return null;
                            }),
                      ),
                      const FieldPhone(),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).size.height.round() *
                                0.03),
                        child: TextFormField(
                            decoration: InputDecoration(
                              labelText: "Contraseña",
                              border: OutlineInputBorder(),
                              suffixIcon: IconButton(
                                icon: Icon(
                                    // Cambia el icono según el estado de isSeePass
                                    password.isSeePass
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                onPressed: () {
                                  context
                                      .read<PrincipleViewModel>()
                                      .changeSeePassword();
                                },
                              ),
                            ),
                            obscureText: password.isSeePass,
                            controller: password.text,
                            keyboardType: TextInputType.text,
                            onChanged: (va) {
                              formkey.currentState!.validate();
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Por favor ingrese una contraseña';
                              } else if (value.length < 8) {
                                return 'La contraseña debe tener al menos 8 caracteres';
                              }
                              return null;
                            }),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).size.height.round() *
                                0.03),
                        child: TextFormField(
                            obscureText: repeatPassword.isSeePass,
                            decoration: InputDecoration(
                              labelText: "Repite la contraseña",
                              border: OutlineInputBorder(),
                              suffixIcon: IconButton(
                                icon: Icon(
                                    // Cambia el icono según el estado de isSeePass
                                    repeatPassword.isSeePass
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                onPressed: () {
                                  context
                                      .read<PrincipleViewModel>()
                                      .changeSeeRepeatPassword();
                                },
                              ),
                            ),
                            controller: repeatPassword.text,
                            keyboardType: TextInputType.text,
                            onChanged: (va) {
                              formkey.currentState?.validate();
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Por favor confirme su contraseña';
                              } else if (repeatPassword.text.text !=
                                  password.text.text) {
                                return 'Las contraseñas no coinciden';
                              }
                              return null;
                            }),
                      ),
                      SizedBox(
                        width: double.infinity,
                        child: Padding(
                          padding: EdgeInsets.only(
                              bottom:
                                  MediaQuery.of(context).size.height.round() *
                                      0.03),
                          child: FilledButton(
                            onPressed: () {
                              onclickValid(formkey, context);
                            },
                            child: const Text("Registrarse"),
                          ),
                        ),
                      )
                    ],
                  )),
            )
          ],
        ));
  }

  void onclickValid(GlobalKey<FormState> formkey, BuildContext context) {
    if (formkey.currentState!.validate()) {
      context.read<PrincipleViewModel>().saveUser().then((result) {
        switch (result) {
          case Left<String, void>():
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text(result.value)),
            );
            break;
          case Right<String, void>():
            if (Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              } else {
                Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => const LoginScreen()),
                  (Route<dynamic> route) => false,
                );
              }
              break;
        }
      });
    }
  }
}

final class FieldName extends StatelessWidget {
  const FieldName({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final name = context.select((PrincipleViewModel value) => value.name);
    final formkey = context.select((PrincipleViewModel value) => value.formkey);
    return Padding(
      padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).size.height.round() * 0.03),
      child: TextFormField(
          decoration: const InputDecoration(
            labelText: "Nombre del Usuario",
            border: OutlineInputBorder(),
            suffixIcon: Icon(Icons.person),
          ),
          obscureText: false,
          style: TextStyle(),
          controller: name,
          onChanged: (va) {
            formkey.currentState!.validate();
          },
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Por favor ingrese su nombre';
            }
            return null; // Retorna null si el valor es válido
          }),
    );
  }
}

final class FieldPhone extends StatelessWidget {
  const FieldPhone({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final phone = context.select((PrincipleViewModel value) => value.phone);
    final formkey = context.select((PrincipleViewModel value) => value.formkey);
    return Padding(
      padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).size.height.round() * 0.03),
      child: TextFormField(
          decoration: const InputDecoration(
            labelText: "Telefono",
            border: OutlineInputBorder(),
            suffixIcon: Icon(Icons.phone_android),
          ),
          controller: phone,
          keyboardType: TextInputType.phone,
          onChanged: (va) {
            formkey.currentState!.validate();
          },
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Por favor ingrese su número de teléfono';
            }
            // Aquí puedes añadir más lógica para validar el formato del teléfono
            return null;
          }),
    );
  }
}
