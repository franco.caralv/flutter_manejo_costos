import 'package:flutter/material.dart';
import 'package:hello_friends/controller/principle_view_model.dart';
import 'package:provider/provider.dart';

class CounterScreen extends StatelessWidget{
  const CounterScreen({super.key});



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      Container(
        margin: EdgeInsets.all(20),
        // Usa BoxDecoration para añadir color y bordes
        decoration: BoxDecoration(
          color: Colors.blue, // Color de fondo del Container
          border: Border.all(
            color: Colors.black, // Color del borde
            width: 3, // Ancho del borde
          ),
          borderRadius: BorderRadius.circular(10), // Bordes redondeados
        ),
        child:Center(  child: Column(
          children:  [
            Container(
              margin: EdgeInsets.all(20),
              // Usa BoxDecoration para añadir color y bordes
              decoration: BoxDecoration(
                color: Colors.blue, // Color de fondo del Container
                border: Border.all(
                  color: Colors.black, // Color del borde
                  width: 3, // Ancho del borde
                ),
                borderRadius: BorderRadius.circular(10), // Bordes redondeado
              ),
              child:Text( context.watch<PrincipleViewModel>().name.text),

            ),
            Container(
              constraints: BoxConstraints.expand(
                height: Theme.of(context).textTheme.headlineMedium!.fontSize! * 1.1 + 200.0,
              ),
              padding: const EdgeInsets.all(8.0),
              color: Colors.blue[600],
              alignment: Alignment.center,
              transform: Matrix4.rotationZ(0.1),
              child: Text('Hello World',
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: Colors.white)),
            )
          ],
        ) ),

      )

    );
  }

}
