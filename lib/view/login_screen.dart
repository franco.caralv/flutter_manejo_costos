import 'package:flutter/material.dart';
import 'package:hello_friends/components/colors.dart';

class LoginScreen extends StatelessWidget {

  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SafeArea(child: ListView(
      children: [
        SizedBox(
          height: 260,
          child:  Image.asset('images/Arbol.jpg', fit: BoxFit.fill)
        ),
        const Padding(padding: EdgeInsets.symmetric(vertical: 10),child: Center(child: Text("Bienvenido a Breezy",
        style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold )))),
        const Padding(padding: EdgeInsets.symmetric(vertical: 5,horizontal: 5),child: Center(child: Text("Gaste con confianza. Estamos aquí para mantener su dinero seguro.",
        textAlign: TextAlign.center))),
        Form(child: Column(children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width.round() *
                    0.02,
              vertical:  MediaQuery.of(context).size.height.round() *
                  0.02
            ),
            child: TextFormField(
                decoration: const InputDecoration(
                  labelText: "Correo",
                  border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  suffixIcon: Icon(Icons.email),
                ),
                obscureText: false,
                keyboardType: TextInputType.emailAddress,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingrese su correo electrónico';
                  } else if (!value.contains('@')) {
                    return 'Por favor ingrese un correo electrónico válido';
                  }
                  return null;
                }),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width.round() *
                    0.02,
                vertical:  MediaQuery.of(context).size.height.round() *
                    0.02
            ),
            child: TextFormField(
                decoration: const InputDecoration(
                  labelText: "Contraseña",
                  border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  suffixIcon: Icon(Icons.visibility),
                ),
                obscureText: false,
                keyboardType: TextInputType.emailAddress,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingrese su correo electrónico';
                  } else if (!value.contains('@')) {
                    return 'Por favor ingrese un correo electrónico válido';
                  }
                  return null;
                }),
          ),

        ])),
         Padding(padding: EdgeInsets.symmetric(vertical: 5,horizontal: MediaQuery.of(context).size.width.round() *
            0.02),child: const Text("¿Has olvidado tu contraseña?",
          textAlign: TextAlign.start,style: TextStyle(color: GreenLight
          ),)),
        SizedBox(
          width: double.infinity,
          child: Padding(
              padding: EdgeInsets.symmetric(
                  vertical:
                  MediaQuery.of(context).size.height.round() *
                      0.01,horizontal: MediaQuery.of(context).size.width.round() *
                  0.02),
            child: FilledButton(
              onPressed: () {

              },
              child: const Text("Iniciar sesión"),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical:
                MediaQuery.of(context).size.height.round() *
                    0.01,horizontal: MediaQuery.of(context).size.width.round() *
                0.02),
            child: FilledButton(
              onPressed: () {

              },
              style: const ButtonStyle(backgroundColor: MaterialStatePropertyAll(GreenMediunLight)),
              child: const Text("¿No tienes una cuenta? Inscribirse",style: TextStyle(color: Colors.white)),
            ),
          ),
        )
      ],
    )) ,);
  }

}
